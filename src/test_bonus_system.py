from bonus_system import calculateBonuses


# for non exist program

def test_incorrect_program():
    assert calculateBonuses("Atest", 10000) == 0
    assert calculateBonuses("testZ", 10000) == 0


# for boundaries
def test_boundaries():
    assert round(calculateBonuses("Standard", 10000), 2) == 0.75
    assert round(calculateBonuses("Standard", 50000), 2) == 1
    assert round(calculateBonuses("Standard", 100000), 2) == 1.25
    assert round(calculateBonuses("Premium", 10000), 2) == 0.15
    assert round(calculateBonuses("Premium", 50000), 2) == 0.2
    assert round(calculateBonuses("Premium", 100000), 2) == 0.25
    assert round(calculateBonuses("Diamond", 10000), 2) == 0.3
    assert round(calculateBonuses("Diamond", 50000), 2) == 0.4
    assert round(calculateBonuses("Diamond", 100000), 2) == 0.5


# for Standard

def test_standard_min_bonus():
    amount = 10000 - 1
    assert round(calculateBonuses("Standard", amount), 2) == 0.5


def test_standard_medium_bonus():
    amount = 50000 - 1
    assert round(calculateBonuses("Standard", amount), 2) == 0.75


def test_standard_high_bonus():
    amount = 100000 - 1
    assert round(calculateBonuses("Standard", amount), 2) == 1


def test_standard_max_bonus():
    amount = 1000000 - 1
    assert round(calculateBonuses("Standard", amount), 2) == 1.25


# for Premium

def test_premium_min_bonus():
    amount = 10000 - 1
    assert round(calculateBonuses("Premium", amount), 2) == 0.1


def test_premium_medium_bonus():
    amount = 50000 - 1
    assert round(calculateBonuses("Premium", amount), 2) == 0.15


def test_premium_high_bonus():
    amount = 100000 - 1
    assert round(calculateBonuses("Premium", amount), 2) == 0.2


def test_premium_max_bonus():
    amount = 1000000 - 1
    assert round(calculateBonuses("Premium", amount), 2) == 0.25


# for "Diamond" program

def test_diamond_min_bonus():
    amount = 10000 - 1
    assert round(calculateBonuses("Diamond", amount), 2) == 0.2


def test_diamond_medium_bonus():
    amount = 50000 - 1
    assert round(calculateBonuses("Diamond", amount), 2) == 0.3


def test_diamond_high_bonus():
    amount = 100000 - 1
    assert round(calculateBonuses("Diamond", amount), 2) == 0.4


def test_diamond_max_bonus():
    amount = 1000000 - 1
    assert round(calculateBonuses("Diamond", amount), 2) == 0.5
