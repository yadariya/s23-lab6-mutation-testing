# Lab6 -- Mutation testing

[![pipeline status](https://gitlab.com/yadariya/s23-lab6-mutation-testing/badges/main/pipeline.svg)](https://gitlab.com/yadariya/s23-lab6-mutation-testing/-/commits/main)

## Introduction

Hey there, today you won't need to develop CI(finally you can relax a bit). We are going to walk through mutation
testing. These testing approach is not that much popular comparing to the one's we've reviewed previously, but in the
same time it is really useful. Don't forget to ask yourself each time before you deciding to test something, do you
really need to use this testing method, and if answer is yes, answer how much resourses are you ready to use for
testing. ***Let's roll!***

## Mutation testing

Mutation testing is a very good tool to check your own tests. It is changing different conditions/cycles and checking if
some of the tests are failing because of it. If some tests are breaking, then nice job, you've developped a good testing
suite.

## Lab

Finally, we may forget our old Java project, let's use something mainstream now, what about python 3?
1. Create your fork of the `
Lab6 - Mutation testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/s23-lab6-fuzz-testing)
2. Now, let's run mutation testing to check if testing that we already have is enough, you can install libs with this command in directory of the project:
```sh
pip3 install -r requirements.txt
```
To run mutation tests you should type `mutatest --src ./src`, it will show you something like this:

```txt
Mutatest diagnostic summary
===========================
 - Source location: /home/danil/s23-lab6-fuzz-testing/src
 - Test commands: ['pytest']
 - Mode: s
 - Excluded files: []
 - N locations input: 10
 - Random seed: None

Random sample details
---------------------
 - Total locations mutated: 10
 - Total locations identified: 21
 - Location sample coverage: 47.62 %


Running time details
--------------------
 - Clean trial 1 run time: 0:00:00.133519
 - Clean trial 2 run time: 0:00:00.124259
 - Mutation trials total run time: 0:00:03.562420

 Overall mutation trial summary
==============================
 - DETECTED: 18
 - SURVIVED: 6
 - TOTAL RUNS: 24
```

As you can see 75% (18/24) of mutants were killed, we can look on the detailed report on bugs in the logs.

## Homework

As a homework you will need to develop tests for function `calculateBonuses`(in `src/bonus_system.py`), then you should be sure that tests are killing 100% of the mutants. After it, you will need to create `.gitlab-ci.yml` and add your tests there. (hint you may use -x parameter to fail pipeline if some amount of mutanst survived)
**Lab is counted as done, if pipelines are passing. and tests are developped**

